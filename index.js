// [SECTION] /////////////////
db.rooms.insertOne({
    name: "Single",
    accomodates: 2,
    price: 1000,
    description: "A simple room with all the basic necessities",
    rooms_available: 10,
    isAvailable: false
})

// [SECTION] /////////////////
db.rooms.insertMany([
    {
    "name": "Double",
    "accomodates": 2,
    "price": 1000,
    "description": "A budget-friendly room with basic amenities",
    "rooms_available": 10,
    "isAvailable": false
    },
    {
    "name": "Queen",
    "accomodates": 4,
    "price": 2000,
    "description": "A spacious room with all the modern amenities",
    "rooms_available": 5,
    "isAvailable": true
    },
    {
    "name": "King",
    "accomodates": 1,
    "price": 500,
    "description": "A small room with basic facilities",
    "rooms_available": 8,
    "isAvailable": true
    }
])
// [SECTION] /////////////////
db.rooms.find({name: "double"})

// [SECTION] /////////////////
db.rooms.updateOne({name:"Queen"},{$set:{rooms_available:0}})

// [SECTION] /////////////////
db.rooms.deleteMany({rooms_available:0})